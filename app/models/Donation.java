package models;


import javax.persistence.Entity;

import play.db.jpa.Model;
import java.util.List;
import java.util.ArrayList;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;


@Entity
public class Donation extends Model
{
	public long received;
	public String methodDonated;
	public Date dateDonated;
	
	@ManyToOne
	public User from;
	
	public Donation(User from, long received, String methodDonated)
	{
		this.received = received;
		this.methodDonated = methodDonated;
		this.from = from;
		dateDonated = new Date();
	}
	}

	
	
	
	

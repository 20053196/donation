package models;


import javax.persistence.CascadeType;
import javax.persistence.Entity;

import play.db.jpa.Model;
import java.util.List;
import java.util.ArrayList;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
public class User extends Model
{
	public boolean usCitizen;
	public String firstName;
	public String lastName;
	public String email;
	public String password; 
	
	@OneToMany(mappedBy = "from" , cascade=CascadeType.ALL)
	List<Donation> donations= new ArrayList<Donation>();
	
	public User(boolean usCitizen, String firstName, String lastName, String email, String password)
	{
		this.usCitizen = usCitizen;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;

	}
	
	public static User findByEmail(String email)
	{
		return find("email" , email).first();
	}
	
	public boolean checkPassword(String password)
	{
		return this.password.equals(password);
	}
	
	
  
}